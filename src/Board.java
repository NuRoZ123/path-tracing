import java.awt.*;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * class Board
 */
public class Board
{
    private HashMap<String, Integer> listeCase = new HashMap<>();

    /**
     * Board constructor
     */
    public Board()
    {
        for(int x = 0; x < Main.NBRCASEXY; ++x)
        {
            for(int y = 0; y < Main.NBRCASEXY; ++y)
            {
                listeCase.put(x + " " + y, 0);
            }
        }

        int count = Main.NBRCASEBOARD;
        while(count > 0)
        {
            int x =  ThreadLocalRandom.current().nextInt(0, Main.NBRCASEXY);
            int y =  ThreadLocalRandom.current().nextInt(0, Main.NBRCASEXY);

            if(listeCase.get(x + " " + y) == 0)
            {
                listeCase.put(x + " " + y, 1);
                --count;
            }
        }
    }

    /**
     * draw Cube
     * @param g
     */
    public void drawCube(Graphics g)
    {
        g.setColor(Main.box);
        int tailleCube = (Main.WIDTH / 2) / Main.NBRCASEXY;
        for(int x = 0; x < Main.NBRCASEXY; ++x)
        {
            for (int y = 0; y < Main.NBRCASEXY; ++y)
            {
                if(listeCase.get(x + " " + y) == 1)
                {
                    g.fillRect(x * tailleCube, y * tailleCube, tailleCube, tailleCube);
                }
            }
        }
    }

    /**
     * check if position are in box
     * @param pos
     * @return
     */
    public boolean positionAreInBox(Vector pos)
    {
        boolean res = false;
        int tailleCube = (Main.WIDTH / 2) / Main.NBRCASEXY;

        for(int x = 0; x < Main.NBRCASEXY; ++x)
        {
            for (int y = 0; y < Main.NBRCASEXY; ++y)
            {
                if(pos.x > x * tailleCube && pos.x < x * tailleCube + tailleCube && pos.y > y * tailleCube && pos.y < y * tailleCube + tailleCube && listeCase.get(x + " " + y) == 1)
                {
                    res = true;
                    break;
                }
            }
        }

        return res;
    }
}
