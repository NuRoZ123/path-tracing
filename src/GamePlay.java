import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * class GamePlay
 */
public class GamePlay extends JPanel implements KeyListener, ActionListener, MouseListener
{
    private Board board = new Board();

    /**
     * constructor GamePlay
     */
    public GamePlay()
    {
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    /**
     * update ray by mousePosition
     * @return
     */
    public ArrayList<ArrayList<Vector>> updateRays()
    {
        ArrayList<ArrayList<Vector>> liste = new ArrayList<>();

        int nbrRay = (int) ((Main.fov.y - Main.fov.x) * 1 / Main.RAYPRECISION);
        ArrayList<Double> listeAllRayAngle = new ArrayList<Double>();

        for(int i = 0; i < nbrRay; ++i)
        {
            listeAllRayAngle.add(Main.fov.x + (i * Main.RAYPRECISION));
        }

        for(int i = 0; i < listeAllRayAngle.size(); ++i)
        {
            double radians = listeAllRayAngle.get(i) * (Math.PI / 180);
            Vector direction = new Vector(Math.cos(radians) / Main.PRECISION, Math.sin(radians) / Main.PRECISION);

            Vector startPos = new Vector(Main.fenetre.getMousePosition().x, Main.fenetre.getMousePosition().y);

            Vector newPosition = new Vector(Main.fenetre.getMousePosition().x, Main.fenetre.getMousePosition().y);
            newPosition.x += direction.x;
            newPosition.y += direction.y;

            while(newPosition.x > 0 && newPosition.x < Main.WIDTH / 2 && newPosition.y > 0 && newPosition.y < Main.HEIGHT && !(board.positionAreInBox(newPosition)))
            {
                newPosition.x += direction.x;
                newPosition.y += direction.y;
            }

            ArrayList<Vector> startEndPos = new ArrayList<>();
            startEndPos.add(0, startPos);
            startEndPos.add(1, newPosition);

            liste.add(startEndPos);
        }

        return liste;
    }

    /**
     * draw on window
     * @param g
     */
    @Override
    public void paint(Graphics g)
    {
        g.setColor(Main.backGround);
        g.fillRect(0, 0, Main.WIDTH, Main.HEIGHT);

        board.drawCube(g);
        try
        {
            ArrayList<ArrayList<Vector>> rays = drawRay(g);
            drawWall(g, rays);
        }
        catch(Exception e)
        {
            System.out.println("souris hors champ");
        }
    }

    /**
     * draw ray
     * @param g
     * @return
     */
    private ArrayList<ArrayList<Vector>> drawRay(Graphics g)
    {
        ArrayList<ArrayList<Vector>> vision = updateRays();

        for(int i = 0; i < vision.size(); ++i)
        {
            g.setColor(Main.ray);
            g.drawLine((int) vision.get(i).get(0).x - 8, (int) vision.get(i).get(0).y - 30, (int) vision.get(i).get(1).x, (int) vision.get(i).get(1).y);
        }

        return vision;
    }

    /**
     * drow wall
     * @param g
     * @param rays
     */
    private void drawWall(Graphics g, ArrayList<ArrayList<Vector>> rays)
    {
        int nbrRay = rays.size();
        int sizeByRay = (Main.WIDTH / 2) / nbrRay;

        for(int i = 0; i < nbrRay; ++i)
        {
            double distance = Math.sqrt(Math.pow(rays.get(i).get(0).x - rays.get(i).get(1).x, 2) + Math.pow(rays.get(i).get(0).y - rays.get(i).get(1).y, 2));
            int grayColorNbr = (int) (255 * (1 - distance / 860));
            Color grayColor = new Color(grayColorNbr, grayColorNbr, grayColorNbr);
            double hauteur = Main.HEIGHT * (1 - distance / 860);

            g.setColor(grayColor);
            g.fillRect((int) (600 + i * sizeByRay), (int) ((Main.HEIGHT - hauteur)/2), (int) (sizeByRay), (int)(hauteur));
        }
    }

    //not use
    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    /**
     * change fov (left, right)
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
        {
            Main.fov = new Vector(Main.fov.x - Main.SPEEDROTATION, Main.fov.y - Main.SPEEDROTATION);
        }
        else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            Main.fov = new Vector(Main.fov.x + Main.SPEEDROTATION, Main.fov.y + Main.SPEEDROTATION);
        }
    }

    /**
     * close or refresh board (escape, f5)
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            Main.run = false;
        }
        else if(e.getKeyCode() == KeyEvent.VK_F5)
        {
            board = new Board();
        }
    }

    //not use
    @Override
    public void actionPerformed(ActionEvent e)
    {

    }

    //not use
    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {

    }

    //not use
    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {

    }

    //not use
    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {

    }

    //not use
    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    //not use
    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }
}