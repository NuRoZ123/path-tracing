import javax.swing.*;
import java.awt.*;

/**
 * class Main
 */
public class Main
{
    //setting
    public final static int NBRCASEXY = 12;
    public final static int NBRCASEBOARD = 25;
    public final static int HEIGHT = 600;
    public final static int WIDTH = HEIGHT * 2;
    public final static int PRECISION = 1;
    public final static double RAYPRECISION = 0.5;
    public final static int SPEEDROTATION = 5;
    public final static Color ray = new Color(255, 255, 255);
    public final static Color backGround = new Color(0, 0, 0);
    public final static Color box = new Color(255, 255, 255);

    //fenetre et jeu
    public static JFrame fenetre = new JFrame();
    public static boolean run = true;
    public static GamePlay gamePlay = new GamePlay();

    //var
    public static Vector fov = new Vector(0, 60);

    /**
     * main
     * @param args
     */
    public static void main(String[] args)
    {
        //parametre fenetre
        fenetre.setBounds(10,10,WIDTH,HEIGHT);
        fenetre.setTitle("path tracing - NuRoZ");
        fenetre.setResizable(false);
        fenetre.setVisible(true);
        fenetre.setAlwaysOnTop(true);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.add(gamePlay);

        //framerate
        final int TICKS_PER_SECOND = 60;
        final int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
        final int MAX_FRAMESKIP = 5;
        double next_game_tick = System.currentTimeMillis();
        int loops;
        while(run)
        {
            loops = 0;
            while (System.currentTimeMillis() > next_game_tick && loops < MAX_FRAMESKIP)
            {
                gamePlay.repaint();

                next_game_tick += SKIP_TICKS;
                loops++;
            }

        }

        fenetre.dispose();
    }
}
